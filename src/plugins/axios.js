import axios from "axios";
import Vue from "vue";

const API = axios.create({
  baseURL: "https://pasarikan.herokuapp.com/",
  headers: {
    "Content-Type": "application/json"
    // Accept: "application/json"
  }
});

export default {
  install() {
    Vue.prototype.$my_api_style = API;
  }
};
