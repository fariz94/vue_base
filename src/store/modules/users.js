// import shop from '../../api/shop'

// initial state
const state = () => ({
  users_data: []
});

// getters
const getters = {};

// actions
const actions = {
  getAllList({ commit }, payload) {
    commit("setUsers", payload);
  },
  addUsers({ commit }, payload) {
    commit("addUsers", payload);
  },
  deleteUser({ commit }, _id) {
    commit("deleteUser", _id);
  }
};

// mutations
const mutations = {
  setUsers(state, payload) {
    state.users_data = payload;
  },
  addUsers(state, payload) {
    state.users_data.push(payload);
  },
  deleteUser(state, _id) {
    state.users_data = state.users_data.filter(el => el._id !== _id);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
