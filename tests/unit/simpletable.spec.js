import { mount } from "@vue/test-utils";
import TablesList from "@/components/Tables/SimpleTable/TableList.vue";

describe("Input", () => {
  // Now mount the component and you have the wrapper
  const wrapper = mount(TablesList);

  it("renders the correct input type", () => {
    expect(wrapper.html()).toContain('<span class="count">0</span>');
  });

  // it's also easy to check for the existence of elements
  it("has a button", () => {
    expect(wrapper.contains("button")).toBe(true);
  });
});
